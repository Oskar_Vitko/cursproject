﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kunashko_Kurs
{
    [Serializable]
    class Admin : IAccount
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
