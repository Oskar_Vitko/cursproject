﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Kunashko_Kurs
{
    class EnterInAccount
    {
        public static void Registration()
        {
            bool c = false;
            User active = new User();
            List<User> users = SaveAndLoad.LoadFromFile<User>("accounts/user.dat");           
            while (true)
            {
                c = false;
                Console.WriteLine("Введите логин");
                active.Login=Console.ReadLine();
                foreach (User u in users)
                    if (active.Login==u.Login)
                    {
                        c = true;
                        Console.WriteLine("Данный логин уже занят! Повторите ввод!");
                        break;
                    }
                if (c==false) break;
            }
            Console.WriteLine("Введите пароль");
            active.Password=Console.ReadLine();
            users.Add(active);
            Console.WriteLine("Учётная запись успешно создана");
            SaveAndLoad.SaveInFile(users, "accounts/user.dat");
        }   
        public static void EnterAdmin()
        {
            List<Admin> adminInfo = SaveAndLoad.LoadFromFile<Admin>("accounts/admin.dat");
            Admin active = new Admin();
            Console.WriteLine("Введите логин");
            active.Login=Console.ReadLine();
            Console.WriteLine("Введите пароль");
            active.Password=Console.ReadLine();
            if (active.Password == adminInfo[0].Password && active.Login == adminInfo[0].Login)
            {
                Console.Clear();
                Console.WriteLine("Вы успешно вошли!");
                MenuForAdmin.Menu(ref active);
            }
            else Console.WriteLine("Неверный логин или пароль!");
        }

        public static void EnterUser()
        {   bool c = false;
            User active = new User();
            List<User> users = SaveAndLoad.LoadFromFile<User>("accounts/user.dat");
            Console.WriteLine("Введите логин");
            active.Login=Console.ReadLine();
            Console.WriteLine("Введите пароль");
            active.Password=Console.ReadLine();
            foreach (User u in users)
            {
                if (active.Login == u.Login && active.Password == u.Password)
                {
                    c = true;
                    Console.Clear();
                    Console.WriteLine("Вы успешно вошли!");
                    MenuForUser.Menu();
                    break;
                }
            }
            if (c == false) Console.WriteLine("Неверный логин или пароль!");

        }
    }
}
