﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Kunashko_Kurs
{
    class SaveAndLoad
    {
        public static void SaveInFile<T>(List<T> list, string path)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (var stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(stream, list);
            }
        }
        public static List<T> LoadFromFile<T>(string path)
        {
            List<T> list = new List<T>();
            BinaryFormatter formatter = new BinaryFormatter();
            using (var stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                if (stream.Length!=0)
                list = (List<T>)formatter.Deserialize(stream);
            }
            return list;
        }       
    }
}
