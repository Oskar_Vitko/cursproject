﻿using System;

namespace Kunashko_Kurs
{
    class Menu
    {
        static void Main(string[] args)
        {
                while (true)
                {
                    Console.WriteLine("1. Войти под Администратором\n2. Войти под Пользователем\n3. Регистрация\n4. Выход\n");
                    switch (Console.ReadLine())
                    {
                        case "1": EnterInAccount.EnterAdmin(); break;
                        case "2": EnterInAccount.EnterUser(); break;
                        case "3": EnterInAccount.Registration(); break;
                        case "4": return;
                        default: Console.WriteLine("Ошибка! Повторите 1-4!"); break;
                    }
                }
        }
    }
}
