﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Kunashko_Kurs
{
    class LoadAndSave
    {
        public static void Load(List<IFirm> firm)
        {
            try
            {
                BinaryReader bw = new BinaryReader(File.Open("files/readers.dat", FileMode.OpenOrCreate));
                while (bw.BaseStream.Position != bw.BaseStream.Length)
                {
                    Reader c = new Reader();
                    c.SecName = bw.ReadString();
                    c.Name = bw.ReadString();
                    c.ThirdName = bw.ReadString();
                    c.Adres = bw.ReadString();
                    c.Number = bw.ReadString();
                    firm.Add(c);
                }
                bw.Close();
                bw = new BinaryReader(File.Open("files/books.dat", FileMode.OpenOrCreate));
                while (bw.BaseStream.Position != bw.BaseStream.Length)
                {
                    Book p = new Book();
                    p.Name = bw.ReadString();
                    p.Janr = bw.ReadString();
                    p.Price = bw.ReadDouble();
                    p.Arenda = bw.ReadDouble();
                    firm.Add(p);
                }
                bw.Close();
                bw = new BinaryReader(File.Open("files/ibooks.dat", FileMode.OpenOrCreate));
                while (bw.BaseStream.Position != bw.BaseStream.Length)
                {
                    IBooks v = new IBooks();
                    v.DateIn = bw.ReadString();
                    v.DateOut = bw.ReadString();
                    v.VReader = new Reader();
                    v.VBook = new Book();
                    v.VReader.SecName = bw.ReadString();
                    v.VReader.Name = bw.ReadString();
                    v.VBook.Name = bw.ReadString();
                    firm.Add(v);
                }
                bw.Close();
            }
            catch { Console.WriteLine("Файл повреждён или пуст!"); }
        }
        public static void Save(List<IFirm> firm)
        {
            try
            {
                BinaryWriter bw = new BinaryWriter(File.Open("files/readers.dat", FileMode.OpenOrCreate));
                Reader c = new Reader();
                for (int i = 0; i < firm.Count; i++)
                    if (firm[i] is Reader)
                    {
                        c = (Reader)firm[i];
                        bw.Write(c.SecName);
                        bw.Write(c.Name);
                        bw.Write(c.ThirdName);
                        bw.Write(c.Adres);
                        bw.Write(c.Number);
                    }
                bw.Close();
                bw = new BinaryWriter(File.Open("files/books.dat", FileMode.OpenOrCreate));
                Book p = new Book();
                for (int i = 0; i < firm.Count; i++)
                    if (firm[i] is Book)
                    {
                        p = (Book)firm[i];
                        bw.Write(p.Name);
                        bw.Write(p.Janr);
                        bw.Write(p.Price);
                        bw.Write(p.Arenda);
                    }
                bw.Close();
                bw = new BinaryWriter(File.Open("files/ibooks.dat", FileMode.OpenOrCreate));
                IBooks v = new IBooks();
                for (int i = 0; i < firm.Count; i++)
                    if (firm[i] is IBooks)
                    {
                        v = (IBooks)firm[i];
                        bw.Write(v.DateIn);
                        bw.Write(v.DateOut);
                        bw.Write(v.VReader.SecName);
                        bw.Write(v.VReader.Name);
                        bw.Write(v.VBook.Name);
                    }
                bw.Close();
            }

            catch { Console.WriteLine("Ошибка записи данных!"); }
        }
    }
}
