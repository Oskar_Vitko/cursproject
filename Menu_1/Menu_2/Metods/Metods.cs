﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kunashko_Kurs
{
    class Metods
    {
        public static void Add(List<IFirm> firm)
        {
            List<Reader> Readers = new List<Reader>();
            for (int i = 0; i < firm.Count; i++)
                if (firm[i] is Reader)
                    Readers.Add((Reader)firm[i]);
            List<Book> putevki = new List<Book>();
            for (int i = 0; i < firm.Count; i++)
                if (firm[i] is Book)
                    putevki.Add((Book)firm[i]);
            while (true)
            {
                Console.WriteLine("1. Читателя\n2. Книгу\n3. Выдать книгу\n4. Выход");
                switch (Console.ReadLine())
                {
                    case "1":
                        Reader c = new Reader();
                        c.Add();
                        firm.Add(c);
                        return;
                    case "2":
                        Book p = new Book();
                        p.Add();
                        firm.Add(p);
                        return;
                    case "3":
                        bool c1 = false, c2 = false;
                        foreach (Reader C in Readers)
                            C.Show();
                        Console.WriteLine("\nВведите ФАМИЛИЮ клиента которому хотите выдать книгу\n");
                        string SecName = Console.ReadLine();
                        Console.WriteLine("\nВведите ИМЯ клиента которому хотите выдать книгу\n");
                        string Name = Console.ReadLine();
                        foreach (Reader C in Readers)
                        {
                            c1 = false;
                            if (Name == C.Name && SecName == C.SecName)
                            {
                                c1 = true;
                                foreach (Book P in putevki)
                                    P.Show();
                                Console.WriteLine("Введите название книги которую хотите выдать");
                                Name = Console.ReadLine();
                                foreach (Book P in putevki)
                                {
                                    c2 = false;
                                    if (Name == P.Name)
                                    {
                                        c2 = true;
                                        IBooks v = new IBooks();
                                        v.Add(P, C);
                                        firm.Add(v);
                                        break;
                                    }
                                }
                                if (c2 == false) Console.WriteLine("Такой книги нет!");
                                break;
                            }
                            if (c1 == false) Console.WriteLine("Такого читателя нет!");
                        }                      
                        return;
                    case "4": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-4!"); break;
                }
            }
        }
        public static void Edit(List<IFirm> firm)
        {
            while (true)
            {
                Console.WriteLine("1. Читателя\n2. Книгу\n3. Выданную книгу\n4. Выход");
                switch (Console.ReadLine())
                {
                    case "1":
                        Reader.Edit(firm);
                                return;
                    case "2":
                        Book.Edit(firm);
                        return;
                    case "3":
                        IBooks.Edit(firm);
                        return;
                    default: Console.WriteLine("Ошибка! Повторите 1-4!"); break;
                }
            }
        }
        public static void Show(List<IFirm> firm)
        {
            while (true)
            {
                Console.WriteLine("1. Читателей\n2. Книги\n3. Выданные книги\n4. Выход");
                switch (Console.ReadLine())
                {
                    case "1":
                        foreach (IFirm f in firm) if (f is Reader) f.Show();
                        return;
                    case "2":
                        foreach (IFirm f in firm) if (f is Book) f.Show();

                        return;
                    case "3":
                        foreach (IFirm f in firm) if (f is IBooks) f.Show();
                        return;
                    case "4": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-4"); break;

                }
            }
        }
        public static void Delete(List<IFirm> firm)
        {
            bool c1 = false;
            string Name, SecName, NameP;
            while (true)
            {
                Console.WriteLine("1. Читателя\n2. Книгу\n3. Выданную книгу\n4. Выход");
                switch (Console.ReadLine())
                {
                    case "1":
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Reader) firm[i].Show();
                        Console.WriteLine("\nВведите ФАМИЛИЮ клиента\n");
                        SecName = Console.ReadLine();
                        Console.WriteLine("\nВведите ИМЯ клиента\n");
                        Name = Console.ReadLine();
                        Reader c = new Reader();
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Reader)
                            {
                                c1 = false;
                                c = (Reader)firm[i];
                                if (Name == c.Name && SecName == c.SecName)
                                {
                                    c1 = true;
                                    firm.Remove(firm[i]);
                                    break;
                                }
                            }
                        if (c1 == false) Console.WriteLine("Такого читателя НЕТ!");
                        return;
                    case "2":
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Book) firm[i].Show();
                        Console.WriteLine("\nВведите НАЗВАНИЕ книги\n");
                        Name = Console.ReadLine();
                        Book p = new Book();
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Book)
                            {
                                c1 = false;
                                p = (Book)firm[i];
                                if (Name == p.Name)
                                {
                                    c1 = true;
                                    firm.Remove(firm[i]);
                                    break;
                                }
                            }
                        if (c1 == false) Console.WriteLine("Такой книги нет!");
                        return;
                    case "3":
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is IBooks) firm[i].Show();
                        Console.WriteLine("\nВведите ФАМИЛИЮ читателя\n");
                        SecName = Console.ReadLine();
                        Console.WriteLine("\nВведите ИМЯ читателя\n");
                        Name = Console.ReadLine();
                        Console.WriteLine("\nВведите НАЗВАНИЕ книги\n");
                        NameP = Console.ReadLine();
                        IBooks v = new IBooks();
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is IBooks)
                            {
                                c1 = false;
                                v = (IBooks)firm[i];
                                if (Name == v.VReader.Name && SecName == v.VReader.SecName && v.VBook.Name == NameP)
                                {
                                    c1 = true;
                                    firm.Remove(firm[i]);
                                    break;
                                }
                            }
                        if (c1 == false) Console.WriteLine("Ошибка в ведённых данных!");
                        return;
                    case "4": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-4!"); break;
                }
            }
        }
        public static void Sort(List<IFirm> firm)
        {
            while (true)
            {
                Console.WriteLine("1. Читателей\n2. Книги\n3. Выданные книги\n4. Выход");
                switch (Console.ReadLine())
                {
                    case "1":
                        Reader.Sort(firm);
                        return;
                    case "2":
                        Book.Sort(firm);
                        return;
                    case "3":
                        IBooks.Sort(firm);
                        return;
                    case "4": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-4!");
                        break;
                }
            }
        }
        public static void Find(List<IFirm> firm)
        {
            while (true)
            {
                Console.WriteLine("1. Читателя\n2. Книгу\n3. Выданную книгу\n4. Выход");
                switch (Console.ReadLine())
                {
                    case "1":
                        Reader.Find(firm);
                        return;
                    case "2":
                        Book.Find(firm);
                        return;
                    case "3":
                        IBooks.Find(firm);
                        return;
                    case "4": return;
                    default:
                        Console.WriteLine("Ошибка! Повторите 1-4!");
                        break;
                }
            }
        }
    }
}
