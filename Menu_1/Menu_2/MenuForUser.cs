﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kunashko_Kurs
{
    class MenuForUser
    {
        public static void Menu()
        {
            List<IFirm> firm = new List<IFirm>();
            LoadAndSave.Load(firm);
            while (true)
            {
                Console.WriteLine($"1. Отобразить список\n2. Сортировка\n3. Поиск\n4. Выход\n");
                switch (Console.ReadLine())
                {
                    case "1":Metods.Show(firm); break;
                    case "2":Metods.Sort(firm); break;
                    case "3":Metods.Find(firm); break;
                    case "4": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-4!"); break;
                }
            }
        }
    }
}
