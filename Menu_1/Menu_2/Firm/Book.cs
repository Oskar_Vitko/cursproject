﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kunashko_Kurs
{
    class Book : IFirm
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public double Arenda { get; set; }
        public string Janr { get; set; }
        public void Add()
        {
            Console.WriteLine("Введите название книги");
            Name = Console.ReadLine();
            while (true)
            {
                try
                {
                    Console.WriteLine("Введите залоговую стоимость");
                    Price = Convert.ToDouble(Console.ReadLine());
                    break;
                }
                catch { Console.WriteLine("Неверный формат данных! Повторите ввод!"); continue; }
            }
            while (true)
            {
                try
                {
                    Console.WriteLine("Введите стоимость аренды");
                    Arenda = Convert.ToDouble(Console.ReadLine());
                    break;
                }
                catch { Console.WriteLine("Неверный формат данных! Повторите ввод!"); continue; }
            }
            Console.WriteLine("Введите жанр");
            Janr = Console.ReadLine();
        }
        public void Show()
        {
            Console.WriteLine($"Название: {Name}\nЖанр: {Janr}\nЗалоговая стоимость: {Price}\nСтоимость аренды: {Arenda}\n---------");
        }
        public static void Edit(List<IFirm> firm)
        {
            string Name; bool c1 = false; Book book = new Book();
            foreach (IFirm b in firm) if (b is Book) b.Show();
            Console.WriteLine("\nВведите НАЗВАНИЕ книги\n");
            Name = Console.ReadLine();
            for (int i = 0; i < firm.Count; i++)
                if (firm[i] is Book)
                {
                    c1 = false;
                    book = (Book)firm[i];
                    if (Name == book.Name)
                    {
                        c1 = true;
                        while (true)
                        {
                            Console.WriteLine("1. Название\n2. Жанр\n3. Залоговую стоимость\n4. Стоимость аренды\n5. Выход");
                            switch (Console.ReadLine())
                            {
                                case "1":
                                    Console.WriteLine("Введите новое НАЗВАНИЕ");
                                    book.Name = Console.ReadLine();
                                    firm[i] = book;
                                    return;
                                case "2":
                                    Console.WriteLine("Введите новый ЖАНР");
                                    book.Janr = Console.ReadLine();
                                    firm[i] = book;
                                    return;
                                case "3":
                                    Console.WriteLine("Введите новую ЗАЛОГОВУЮ СТОИМОСТЬ");
                                    book.Price = Convert.ToDouble(Console.ReadLine());
                                    firm[i] = book;
                                    return;
                                case "4":
                                    Console.WriteLine("Введите новую СТОИМОСТЬ АРЕНДЫ");
                                    book.Arenda = Convert.ToDouble(Console.ReadLine());
                                    firm[i] = book;
                                    return;
                                case "5": return;
                                default: Console.WriteLine("Ошибка! Повторите 1-5!"); break;
                            }
                        }
                    }
                }
            if (c1 == false) Console.WriteLine("Такой книги нет!");

        }
        public static void Sort(List<IFirm> firm)
        {
            List<Book> books = new List<Book>();
            for (int i = 0; i < firm.Count; i++)
                if (firm[i] is Book)
                {
                    books.Add((Book)firm[i]);
                    firm.Remove(firm[i]);
                }
            while (true)
            {
                Console.WriteLine("1. Названию\n2. Жанру\n3. Залоговой стоимости\n4. Стоимости аренды\n5. Выход");
                switch (Console.ReadLine())
                {
                    case "1":
                        books.Sort((x, y) => x.Name.CompareTo(y.Name));
                        firm.AddRange(books);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Book)
                                firm[i].Show();
                        return;
                    case "2":
                        books.Sort((x, y) => x.Janr.CompareTo(y.Janr));
                        firm.AddRange(books);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Book)
                                firm[i].Show();
                        return;
                    case "3":
                        books.Sort((x, y) => x.Price.CompareTo(y.Price));
                        books.Reverse();
                        firm.AddRange(books);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Book)
                                firm[i].Show();
                        return;
                    case "4":
                        books.Sort((x, y) => x.Arenda.CompareTo(y.Arenda));
                        books.Reverse();
                        firm.AddRange(books);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Book)
                                firm[i].Show();
                        return;
                    case "5": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-5!"); break;
                }
            }
        }
        public static void Find(List<IFirm> firm)
        {
            Book p = new Book();
            bool c1 = false;
            string name;
            Console.WriteLine("Введите НАЗВАНИЕ книги");
            name = Console.ReadLine();
            for (int i = 0; i < firm.Count; i++)
            {
                c1 = false;
                if (firm[i] is Book)
                {
                    p = (Book)firm[i];
                    if (name == p.Name)
                    {
                        c1 = true;
                        Console.WriteLine("Найдено");
                        p.Show();
                        break;
                    }
                }
            }
            if (c1 == false) Console.WriteLine("Не найдено");
        }
    }
}
