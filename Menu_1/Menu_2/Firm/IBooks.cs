﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kunashko_Kurs
{
    class IBooks : IFirm
    {
        public Book VBook {get; set;}
        public Reader VReader { get; set; }
        public string DateIn { get; set; }
        public string DateOut { get; set; }
        public void Add(Book p, Reader c)
        {
            VBook = p;
            VReader = c;
            while (true)
            {
                char[] g = Convert.ToString(DateTime.Now).ToCharArray();
                char[] str = new char[10];
                for (int i = 0; i < 10; i++)
                {
                    str[i] = g[i];
                }
                DateIn = new string(str);
                Console.WriteLine("Введите новую ДАТУ ВОЗВРАТА");
                DateOut = Console.ReadLine();
                if (String.Compare(DateOut, DateIn) == -1)
                {
                    Console.WriteLine("Дата возврата не может быть раньше даты выдачи! Повторите ввод!");
                    continue;
                }
                break;
            }         
        }
        public void Show()
        {
            Console.WriteLine($"Читатель: {VReader.SecName} {VReader.Name} {VReader.ThirdName}\n"+
                $"Книга: {VBook.Name}\nДата выдачи: {DateIn}\nДата возврата: {DateOut}\n----------------------");
        }
        public static void Edit(List<IFirm> firm)
        {
            string Name, SecName, NameBook; bool c1 = false; IBooks IBooks = new IBooks();
            foreach (IFirm b in firm) if (b is IBooks) b.Show();
            Console.WriteLine("\nВведите ИМЯ читателя\n");
            Name = Console.ReadLine();
            Console.WriteLine("\nВведите ФАМИЛИЮ читателя\n");
            SecName = Console.ReadLine();
            Console.WriteLine("\nВведите НАЗВАНИЕ книги\n");
            NameBook = Console.ReadLine();
            for (int i = 0; i < firm.Count; i++)
                if (firm[i] is IBooks)
                {
                    c1 = false;
                    IBooks = (IBooks)firm[i];
                    if (Name == IBooks.VReader.Name && SecName == IBooks.VReader.SecName && IBooks.VBook.Name == NameBook)
                    {
                        c1 = true;
                        while (true)
                        {
                            Console.WriteLine("1. Дату возврата\n2. Выход");
                            switch (Console.ReadLine())
                            {
                                case "1":
                                    while (true)
                                    {
                                        Console.WriteLine("Введите новую ДАТУ ВОЗВРАТА");
                                        IBooks.DateOut = Console.ReadLine();
                                        if (String.Compare(IBooks.DateOut,IBooks.DateIn)==-1)
                                        {
                                            Console.WriteLine("Дата возврата не может быть раньше даты выдачи! Повторите ввод!");
                                            continue;
                                        }
                                        break;
                                    }
                                    firm[i] = IBooks;                                   
                                    return;
                                case "2": return;
                                default: Console.WriteLine("Ошибка! Повторите 1-2!"); break;
                            }
                        }
                    }
                }
            if (c1 == false) Console.WriteLine("Такой выданной книги или читателя нет!");

        }
        public static void Sort(List<IFirm> firm)
        {
            List<IBooks> vakansii = new List<IBooks>();
            for (int i = 0; i < firm.Count; i++)
                if (firm[i] is IBooks)
                {
                    vakansii.Add((IBooks)firm[i]);
                    firm.Remove(firm[i]);
                }
            while (true)
            {
                Console.WriteLine("1. Дате выдачи\n2. Дате возврата\n3. Выход");
                switch (Console.ReadLine())
                {
                    case "1":
                        vakansii.Sort((x, y) => x.DateIn.CompareTo(y.DateIn));
                        firm.AddRange(vakansii);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is IBooks)
                                firm[i].Show();
                        return;
                    case "2":
                        vakansii.Sort((x, y) => x.DateOut.CompareTo(y.DateOut));
                        firm.AddRange(vakansii);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is IBooks)
                                firm[i].Show();
                        return;
                    case "3": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-3!"); break;
                }
            }
        }
        public static void Find(List<IFirm> firm)
        {
            IBooks v = new IBooks();
            bool c1 = false;
            string name, secName, NameP;
            Console.WriteLine("Введите ФАМИЛИЮ читателя");
            secName = Console.ReadLine();
            Console.WriteLine("Введите ИМЯ читателя");
            name = Console.ReadLine();
            Console.WriteLine("Введите НАЗВАНИЕ книги");
            NameP = Console.ReadLine();
            for (int i = 0; i < firm.Count; i++)
            {
                c1 = false;
                if (firm[i] is IBooks)
                {
                    v = (IBooks)firm[i];
                    if (name == v.VReader.Name && secName == v.VReader.SecName && NameP==v.VBook.Name)
                    {
                        c1 = true;
                        Console.WriteLine("Найдено");
                        v.Show();
                        break;
                    }
                }
            }
            if (c1 == false) Console.WriteLine("Не найдено");
        }
    }
}
