﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kunashko_Kurs
{
    class Reader : IFirm
    {
        public string Name { get; set; }
        public string SecName { get; set; }
        public string ThirdName { get; set; }
        public string Adres { get; set; }
        public string Number { get; set; }
        public void Add()
        {
            Console.WriteLine("Введите имя");
            Name = Console.ReadLine();
            Console.WriteLine("Введите фамилию");
            SecName = Console.ReadLine();
            Console.WriteLine("Введите отчество");
            ThirdName = Console.ReadLine();
            Console.WriteLine("Введите адрес");
            Adres = Console.ReadLine();
            Console.WriteLine("Введите номер телефона");
            Number = Console.ReadLine();
        }
        public void Show()
        {
            Console.WriteLine($"{SecName} {Name} {ThirdName}\nАдрес: {Adres}\nНомер: {Number}\n---------");
        }
        public static void Edit(List<IFirm> firm)
        {
            string Name, SecName; bool c1 = false; Reader Reader = new Reader();
            foreach (IFirm b in firm) if (b is Reader) b.Show();
            Console.WriteLine("\nВведите ИМЯ читателя\n");
            Name = Console.ReadLine();
            Console.WriteLine("\nВведите ФАМИЛИЮ читателя\n");
            SecName = Console.ReadLine();
            for (int i = 0; i < firm.Count; i++)
                if (firm[i] is Reader)
                {
                    c1 = false;
                    Reader = (Reader)firm[i];
                    if (Name == Reader.Name && SecName == Reader.SecName)
                    {
                        c1 = true;
                        while (true)
                        {
                            Console.WriteLine("1. Имя\n2. Фамилию\n3. Отчество\n4. Адрес\n5. Номер\n6. Выход");
                            switch (Console.ReadLine())
                            {
                                case "1":
                                    Console.WriteLine("Введите новое ИМЯ");
                                    Reader.Name = Console.ReadLine();
                                    firm[i] = Reader;
                                    return;
                                case "2":
                                    Console.WriteLine("Введите новую ФАМИЛИЮ");
                                    Reader.SecName = Console.ReadLine();
                                    firm[i] = Reader;
                                    return;
                                case "3":
                                    Console.WriteLine("Введите новое ОТЧЕСТВО");
                                    Reader.ThirdName = Console.ReadLine();
                                    firm[i] = Reader;
                                    return;
                                case "4":
                                    Console.WriteLine("Введите новый АДРЕС");
                                    Reader.Adres = Console.ReadLine();
                                    firm[i] = Reader;
                                    return;
                                case "5":
                                    Console.WriteLine("Введите новый НОМЕР");
                                    Reader.Number = Console.ReadLine();
                                    firm[i] = Reader;
                                    return;
                                case "6": return;
                                default: Console.WriteLine("Ошибка! Повторите 1-6!"); break;
                            }
                        }
                    }
                }
            if (c1 == false) Console.WriteLine("Такой книги нет!");

        }
        public static void Sort(List<IFirm> firm)
        {
            List<Reader> Readers = new List<Reader>();
            for (int i = 0; i < firm.Count; i++)
                if (firm[i] is Reader)
                {
                    Readers.Add((Reader)firm[i]);
                    firm.Remove(firm[i]);
                }
            while (true)
            {
                Console.WriteLine("1. Имени\n2. Фамилии\n3. Отчеству\n4. Адресу\n5. Номеру\n6. Выход");
                switch (Console.ReadLine())
                {
                    case "1":
                        Readers.Sort((x, y) => x.Name.CompareTo(y.Name));
                        firm.AddRange(Readers);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Reader)
                                firm[i].Show();
                        return;
                    case "2":
                        Readers.Sort((x, y) => x.SecName.CompareTo(y.SecName));
                        firm.AddRange(Readers);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Reader)
                                firm[i].Show();
                        return;
                    case "3":
                        Readers.Sort((x, y) => x.ThirdName.CompareTo(y.ThirdName));
                        firm.AddRange(Readers);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Reader)
                                firm[i].Show();
                        return;
                    case "4":
                        Readers.Sort((x, y) => x.Adres.CompareTo(y.Adres));
                        firm.AddRange(Readers);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Reader)
                                firm[i].Show();
                        return;
                    case "5":
                        Readers.Sort((x, y) => x.Number.CompareTo(y.Number));
                        firm.AddRange(Readers);
                        for (int i = 0; i < firm.Count; i++)
                            if (firm[i] is Reader)
                                firm[i].Show();
                        return;
                    case "6": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-6!"); break;
                }
            }
        }

        public static void Find(List<IFirm> firm)
        {
            Reader c = new Reader();
            bool c1=false;
            string name, secName;
            Console.WriteLine("Введите ФАМИЛИЮ читателя");
            secName = Console.ReadLine();
            Console.WriteLine("Введите ИМЯ читателя");
            name = Console.ReadLine();
            for (int i = 0; i < firm.Count; i++)
            {
                c1 = false;
                if (firm[i] is Reader)
                {
                    c = (Reader)firm[i];
                    if (name == c.Name && secName == c.SecName)
                    {
                        c1 = true;
                        Console.WriteLine("Найдено");
                        c.Show();
                        break;
                    }
                }
            }
            if (c1 == false) Console.WriteLine("Не найдено");
        }
    }
}
