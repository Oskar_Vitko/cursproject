﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Kunashko_Kurs
{
    class WorkWithUsers
    {
        public static void Menu(ref Admin active)
        {
            while (true)
            {
                Console.WriteLine($"Администратор: {active.Login}\n1. Изменить пароль админа\n2. Именить пароль пользователя\n3. Изменить логин пользователя\n" +
                    "4. Удалить пользователя\n5. Выход");
                switch (Console.ReadLine())
                {
                    case "1": EditPasswordAdmin(ref active); break;
                    case "2": EditUserPassword(); break;
                    case "3": EditUserLogin(); break;
                    case "4": DeleteUser(); break;
                    case "5": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-6!"); break;
                }
            }
        }
        static void EditPasswordAdmin(ref Admin active)
        {
            Console.WriteLine("Введите старый пароль");
            string password = Console.ReadLine();
            if (password == active.Password)
            {
                Console.WriteLine("Введите новый пароль");
                active.Password=Console.ReadLine();
                Console.WriteLine("\nПароль успешно изменён!\n");
                SaveAndLoad.SaveInFile(new List<Admin> (){ active}, "accounts/admin.dat");
            }
            else Console.WriteLine("Старый пароль введён неверно!");
        }
        static void EditUserPassword()
        {
            bool c = false;
            List<User> users = SaveAndLoad.LoadFromFile<User>("accounts/user.dat");
            foreach (User u in users)
                Console.WriteLine($"\n{u.Login} пароль: {u.Password}");
            Console.WriteLine("\nВведите логин пользователя которого хотите изменить");
            string login = Console.ReadLine();
            foreach (User u in users)
            {
                if (login == u.Login)
                {
                    c = true;
                    Console.WriteLine("Введите новый пароль");
                    u.Password=Console.ReadLine();
                    SaveAndLoad.SaveInFile(users, "accounts/user.dat");
                    Console.WriteLine("\nПароль успешно изменён!\n");
                    break;
                }
            }
            if (c == false)
            {
                Console.WriteLine("Вы ввели несуществующего пользователя");
                return;
            }
        }
        static void EditUserLogin()
        {
            bool c = false, c2 = false;
            List<User> users = SaveAndLoad.LoadFromFile<User>("accounts/user.dat");
            foreach (User u in users)
                Console.WriteLine($"\n{u.Login} пароль: {u.Password}");
            Console.WriteLine("\nВведите логин пользователя которого хотите изменить");
            string login = Console.ReadLine();
            foreach (User u in users)
            {
                if (login == u.Login)
                {
                    c = true;
                    Console.WriteLine("Введите новый логин");
                    login = Console.ReadLine();
                    foreach (User uu in users)
                    {
                        c2 = false;
                        if (login != uu.Login)
                            c2 = true;
                        else break;
                    }
                    if (c2)
                    {
                        u.Login=login;
                        SaveAndLoad.SaveInFile(users, "accounts/user.dat");
                        Console.WriteLine("\nЛогин успешно изменён!\n");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Данный логин уже занят!"); break;
                    }
                }
            }
            if (c == false)
            {
                Console.WriteLine("Вы ввели несуществующего пользователя");
                return;
            }
        }
        static void DeleteUser()
        {
            bool c = false;
            List<User> users = SaveAndLoad.LoadFromFile<User>("accounts/user.dat");
            foreach (User u in users)
                Console.WriteLine($"\n{u.Login} пароль: {u.Password}");
            Console.WriteLine("\nВведите логин пользователя которого хотите изменить");
            string login = Console.ReadLine();
            foreach (User u in users)
            {
                if (login == u.Login)
                {
                    c = true;
                    Console.WriteLine($"\nПользователь {u.Login} успешно удалён!\n");
                    users.Remove(u);
                    SaveAndLoad.SaveInFile(users, "accounts/user.dat");
                    break;
                }
            }
            if (c == false)
            {
                Console.WriteLine("Вы ввели несуществующего пользователя");
                return;
            }
        }
    }
}
