﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kunashko_Kurs
{
    class MenuForAdmin
    {
        public static void Menu(ref Admin active)
        {
            while (true)
            {
                Console.WriteLine($"Администратор: {active.Login}\n1. Работа с данными\n2. Работа с учётными записями\n3. Выход\n");
                switch (Console.ReadLine())
                {
                    case "1": Work(); break;
                    case "2": WorkWithUsers.Menu(ref active); break;
                    case "3": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-3!"); break;
                }
            }
        }
        public static void Work()
        {
            List<IFirm> firm = new List<IFirm>();
            LoadAndSave.Load(firm);
            while (true)
            {
                Console.WriteLine("1. Добавить запись\n2. Редактировать запись\n3. Удалить запись\n" +
                                          "4. Отобразить список\n5. Сортировка\n6. Поиск\n7. Сохранить данные\n8. Выход");
                switch (Console.ReadLine())
                {
                    case "1": Metods.Add(firm); break;
                    case "2": Metods.Edit(firm); break;
                    case "3": Metods.Delete(firm); break;
                    case "4": Metods.Show(firm); break;
                    case "5": Metods.Sort(firm); break;
                    case "6": Metods.Find(firm); break;
                    case "7": LoadAndSave.Save(firm); break;
                    case "8": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-8!"); break;
                }
            }
        }
    }
}
